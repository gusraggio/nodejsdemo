#!/bin/bash

file="deployment/deployment.yaml"
echo "#last update timestamp "$2 > $file
echo "kind: Deployment" >> $file
echo "apiVersion: apps/v1" >> $file
echo "metadata:" >> $file
echo "  name: frontal" >> $file
echo "  labels:" >> $file
echo "      timestamp: time_$2" >> $file
echo "spec:" >> $file
echo "  replicas: 3" >> $file
echo "  selector:" >> $file
echo "    matchLabels:" >> $file
echo "      app: frontal" >> $file
echo "  template:" >> $file
echo "    metadata:" >> $file
echo "      labels:" >> $file
echo "        app: frontal" >> $file
echo "    spec:" >> $file
echo "      containers:" >> $file
echo "        - name: frontal" >> $file
echo "          image: "$1 >> $file
echo "          imagePullPolicy: Always" >> $file
echo "          ports:" >> $file
echo "            - containerPort: 80" >> $file
echo "      restartPolicy: Always" >> $file
cat $file
